/**
 * \file dna_alignment_on_cpu.cpp
 * \brief ���������� ������������ ��� �� CPU
 *
 *  Copyright (C) 2013  Aleksandr Potapoff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dna_alignment.h"
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include <stdio.h>

static int s(char a, char b)
{
	return (a == b) ? 1 : -1;
}

static int max(int a, int b)
{
	return (a > b) ? a : b;
}

static void fill_matix(const char *seq1, int seq1_len, const  char *seq2,
	int seq2_len, int *f)
{
	int n = seq1_len;
	int m = seq2_len;
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < m; j++) {
			int f11 =	((i > 0) && (j > 0)) ? f[(i - 1) * m + (j - 1)] :
						(j > 0)	? -j * D : -i * D;
			int f10 =	(i > 0) ? f[(i - 1) * m + j] : -(j + 1) * D;
			int f01 =	(j > 0) ? f[i * m + (j - 1)] : -(i + 1) * D;

			f[(i) * m + (j)] = max(max(
								f01 - D,
								f11 + s(seq1[i], seq2[j])),
								f10 - D);
		}
	}
}

static void find_alignment(const char *seq1, int seq1_len,
	const char *seq2, int seq2_len,
	char *seq1_out, char *seq2_out, int *f)
{
	int n = seq1_len;
	int m = seq2_len;

	for (int i = n - 1, j = m - 1, pos = n + m - 1; (i > 0) && (j > 0); pos--) {
		int f10 =	(i > 0) ? f[(i - 1) * m + (j)] : -(j + 1) * D;
		int f11 =	((i > 0) && (j > 0)) ? f[(i - 1) * m + (j - 1)] :
						(j > 0)	? -j * D : -i * D;

		if(f[i * m + j] == f10 - D) {
			seq1_out[pos] = seq1[i--];
			seq2_out[pos] = '-';
		}
		else if(f[i * m + j] == f11 + s(seq1[i], seq2[j])) {
			seq1_out[pos] = seq1[i--];
			seq2_out[pos] = seq2[j--];
		}
		else {
			
			seq1_out[pos] = '-';
			seq2_out[pos] = seq2[j--];
		}
	}
}

int dna_global_alignment_on_cpu(const char *seq1, const char *seq2,
	char *seq1_out, char *seq2_out)
{
	int n = strlen(seq1);
	int m = strlen(seq2);

	int *f = (int*) malloc(n * m * sizeof(int));

	fill_matix(seq1, n, seq2, m, f);

	if ((seq1_out != 0) && (seq2_out != 0))
		find_alignment(seq1, n, seq2, m, seq1_out, seq2_out, f);

	int res = f[m * n - 1];
	free(f);
	return res;
}
