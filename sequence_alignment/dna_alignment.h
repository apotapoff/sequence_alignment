/**
 * \file dna_alignment.h
 * \brief ������� ������������ ���
 *
 *  Copyright (C) 2013, 2014  Aleksandr Potapoff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DNA_ALIGNMENT_H
#define DNA_ALIGNMENT_H

/**
 * \brief ������ ������������ ������������������ ��� � ������� ��������� ��������-����� �� CPU\n
 * ��������� ������������������ � ���� ����� �������� ������������� �������� '\0'\n
 * �������, ������������ ����������: 'a', 'g', 'c', 't'\n
 * �������� ������������������ �������� ����������� ������������� ������� �������������������\n
 * \param[in] seq1 ������ ������� ������������������
 * \param[in] seq2 ������ ������� ������������������
 * \param[out] seq1_out ������ �������� ������������������
 * \param[out] seq2_out ������ �������� ������������������
 * \return ��� ������������ ������������
 */
int dna_global_alignment_on_cpu(const char *seq1, const char *seq2, char *seq1_out, char *seq2_out);

/**
 * \brief ������ ������������ ������������������ ��� � ������� ��������� ��������-����� �� GPU\n
 * ��������� ������������������ � ���� ����� �������� ������������� �������� '\0'\n
 * �������, ������������ ����������: 'a', 'g', 'c', 't'\n
 * �������� ������������������ �������� ����������� ������������� ������� �������������������\n
 * \param[in] threads_per_block ����� ������� � ����� �� GPU ������ ���� ����� ������� 2
 * \param[in] seq1 ������ ������� ������������������
 * \param[in] seq2 ������ ������� ������������������
 * \param[out] seq1_out ������ �������� ������������������
 * \param[out] seq2_out ������ �������� ������������������
 * \return ��� ������������ ������������
 */
int dna_global_alignment_on_gpu(unsigned threads_per_block, const char *seq1, const char *seq2, char *seq1_out, char *seq2_out);

/**
 * \brief ������ ������������ ������������������ ��� � ������� ��������� ��������-����� �� GPU
 * � ������������ �� ������� ������\n
 * ��������� ������������������ � ���� ����� �������� ������������� �������� '\0'\n
 * �������, ������������ ����������: 'a', 'g', 'c', 't'\n
 * �������� ������������������ �������� ����������� ������������� ������� �������������������\n
 * \param[in] threads_per_block ����� ������� � ����� �� GPU ������ ���� ����� ������� 2
 * \param[in] seq1 ������ ������� ������������������
 * \param[in] seq2 ������ ������� ������������������
 * \param[out] seq1_out ������ �������� ������������������
 * \param[out] seq2_out ������ �������� ������������������
 * \return ��� ������������ ������������
 */
int dna_global_alignment_on_gpu_opt(unsigned threads_per_block, const char *seq1, const char *seq2, char *seq1_out, char *seq2_out);

/**
 * \brief ������ ������������ ������������������ ��� � ������� ��������� ��������-����� �� GPU
 * � ������������ �� ������� ������ � ������\n
 * ��������� ������������������ � ���� ����� �������� ������������� �������� '\0'\n
 * �������, ������������ ����������: 'a', 'g', 'c', 't'\n
 * �������� ������������������ �������� ����������� ������������� ������� �������������������\n
 * \param[in] threads_per_block ����� ������� � ����� �� GPU ������ ���� ����� ������� 2
 * \param[in] seq1 ������ ������� ������������������
 * \param[in] seq2 ������ ������� ������������������
 * \param[out] seq1_out ������ �������� ������������������
 * \param[out] seq2_out ������ �������� ������������������
 * \return ��� ������������ ������������
 */
int dna_global_alignment_on_gpu_opt_mem(unsigned threads_per_block, const char *seq1, const char *seq2, char *seq1_out, char *seq2_out);

#endif

