/**
 * \file main.cpp
 * \brief this file is part of sequence_alignment
 * 
 *  Copyright (C) 2013, 2014  Aleksandr Potapoff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda_runtime.h>
#include "dna_sequence.h"
#include "dna_alignment.h"
#include "config.h"

static cudaDeviceProp prop;

static cudaError_t gpu_init()
{
	cudaError_t err = cudaSuccess;
	if ((err = cudaSetDevice(0)) != cudaSuccess)
		return err;
	cudaGetDeviceProperties(&prop, 0);
	return cudaSuccess;
}

static void gpu_Properties()
{
	printf("ASCII string identifying device: %s\n"
			"Global memory available on device in bytes: %u\n"
			"Shared memory available per block in bytes: %u\n"
			"32-bit registers available per block: %d\n"
			"Warp size in threads: %d\n"
			"Maximum pitch in bytes allowed by memory copies: %u\n"
			"Maximum number of threads per block: %d\n"
			"Maximum size of each dimension of a block: { %d, %d, %d }\n"
			"Maximum size of each dimension of a grid: { %d, %d, %d }\n"
			"Clock frequency in kilohertz: %d\n"
			"Constant memory available on device in bytes: %u\n"
			"Major compute capability: %d\n"
			"Minor compute capability: %d\n"
			"Number of multiprocessors on device: %d\n"
			"Peak memory clock frequency in kilohertz: %d\n"
			"Global memory bus width in bits: %d\n"
			"Size of L2 cache in bytes: %d\n"
			"Maximum resident threads per multiprocessor: %d\n",
			prop.name, prop.totalGlobalMem, prop.sharedMemPerBlock, prop.regsPerBlock,
			prop.warpSize, prop.memPitch, prop.maxThreadsPerBlock,
			prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2],
			prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2],
			prop.clockRate, prop.totalConstMem, prop.major, prop.minor,
			prop.multiProcessorCount, prop.memoryClockRate, prop.memoryBusWidth,
			prop.l2CacheSize, prop.maxThreadsPerMultiProcessor);
}

int main(int argc, char **argv)
{
	int result;
	cudaError_t status = gpu_init();
	if (status != cudaSuccess) {
		printf("%s\n", cudaGetErrorString(status));
		return status;
	}
	gpu_Properties();

	printf("\nDNA alignment time tests\n");
	for (unsigned threads_per_block = prop.warpSize;
		threads_per_block <= prop.maxThreadsPerBlock; threads_per_block <<= 1) {
			printf("\nthreads per block: %d\n\n", threads_per_block);

		for (unsigned i = 1; i <= 10; i++) {
			char *s1 = (char*) malloc(i * 1000 + 1);
			char *s2 = (char*) malloc(i * 1000 + 1);

			memcpy(s1, Y13113, i * 1000);
			s1[i * 1000] = '\0';
			memcpy(s2, Y16067, i * 1000);
			s2[i * 1000] = '\0';

			printf("bp: %u\n", i* 1000);

			result = dna_global_alignment_on_gpu(threads_per_block, s1, s2, 0, 0);
			printf("dna_global_alignment_on_gpu: %d\n", result);

			result = dna_global_alignment_on_gpu_opt(threads_per_block, s1, s2, 0, 0);
			printf("dna_global_alignment_on_gpu_opt: %d\n", result);

			result = dna_global_alignment_on_gpu_opt_mem(threads_per_block, s1, s2, 0, 0);
			printf("dna_global_alignment_on_gpu_opt_mem: %d\n", result);

			free(s1);
			free(s2);
		}
	}
	return 0;
}
