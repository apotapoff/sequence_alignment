/**
 * \file dna_alignment_on_gpu.cu
 * \brief ���������� ������������ ��� �� GPU
 *
 *  Copyright (C) 2013, 2014  Aleksandr Potapoff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dna_alignment.h"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <math_functions.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include <stdio.h>

static __host__ __device__ int s(char a, char b)
{
	return (a == b) ? 1 : -1;
}

__global__ void fill_matix_kernel(const char *seq1, const  char *seq2, int *f, int step, int str_count)
{
	extern __shared__ int f01 [];
	int *f11 = &f01[blockDim.x];
	int m = gridDim.x * blockDim.x;
	int j = blockIdx.x * blockDim.x + threadIdx.x;

	int i_start = (step - blockIdx.x) * blockDim.x;
	int i_end = i_start + blockDim.x;

	char c = seq2[j];

	if ((i_start >= 0) && (i_start < str_count)) {
		int f_;
		int f10 =	(i_start > 0) ? f[(i_start - 1) * m + j] : -(j + 1) * D;
		f01[threadIdx.x] = ((blockIdx.x > 0) && (i_start + threadIdx.x < str_count))
			? f[(i_start + threadIdx.x) * m +  blockIdx.x * blockDim.x - 1]
			: -(i_start + threadIdx.x + 1) * D;
		f11[threadIdx.x] =	((i_start > 0) && (j > 0)) ? f[(i_start - 1) * m + (j - 1)] :
							(j > 0)	? -j * D : -i_start * D;
		__syncthreads();
		int i = i_start - threadIdx.x;
		for (int step = 0; step < (2 * blockDim.x - 1); i++, step++) {
			if ((i >= i_start) && (i < i_end)) {
				f_ = max(max(
								f01[i - i_start] - D,
								f11[threadIdx.x] + s(seq1[i], c)),
								f10 - D);
				f[i * m + j] = f_;
				f10 = f_;
			}
			__syncthreads();
			if ((i >= i_start) && (i < i_end))
				f11[threadIdx.x] = f01[i - i_start];
			__syncthreads();
			if ((i >= i_start) && (i < i_end))
				f01[i - i_start] = f_;
			__syncthreads();
		}
	}
}

static void find_alignment(const char *seq1, int seq1_len,
	const char *seq2, int seq2_len,
	char *seq1_out, char *seq2_out, int *f, int col_count)
{
	int n = seq1_len;
	int m = seq2_len;

	for (int i = n - 1, j = m - 1, pos = n + m - 1; (i > 0) && (j > 0); pos--) {
		int f10 =	(i > 0) ? f[(i - 1) * col_count + (j)] : -(j + 1) * D;
		int f11 =	((i > 0) && (j > 0)) ? f[(i - 1) * col_count + (j - 1)] :
						(j > 0)	? -j * D : -i * D;

		if(f[i * col_count + j] == f10 - D) {
			seq1_out[pos] = seq1[i--];
			seq2_out[pos] = '-';
		}
		else if(f[i * col_count + j] == f11 + s(seq1[i], seq2[j])) {
			seq1_out[pos] = seq1[i--];
			seq2_out[pos] = seq2[j--];
		}
		else {
			
			seq1_out[pos] = '-';
			seq2_out[pos] = seq2[j--];
		}
	}
}

int dna_global_alignment_on_gpu(unsigned threads_per_block, const char *seq1, const char *seq2, char *seq1_out, char *seq2_out)
{
	cudaEvent_t start, end;
	int n = strlen(seq1);
	int m = strlen(seq2);

	char *gpu_seq1;
	char *gpu_seq2;
	int *gpu_f;

	int block_count = (m + threads_per_block) / threads_per_block;
	int col_count = block_count * threads_per_block;

	int sblock_count = (n + threads_per_block) / threads_per_block;
	int str_count = sblock_count * threads_per_block;

	int step_count = block_count + sblock_count - 1;

	CUDA_ASSERT(cudaMalloc(&gpu_seq1, str_count));
	CUDA_ASSERT(cudaMemcpy(gpu_seq1, seq1, n, cudaMemcpyHostToDevice));
	CUDA_ASSERT(cudaMalloc(&gpu_seq2, col_count));
	CUDA_ASSERT(cudaMemcpy(gpu_seq2, seq2, m, cudaMemcpyHostToDevice));

	CUDA_ASSERT(cudaMalloc(&gpu_f, str_count * col_count * sizeof(int)));
	
	cudaEventCreate(&start);
	cudaEventCreate(&end);

	cudaEventRecord(start);
	for (int step = 0; step < step_count; step++) {
		fill_matix_kernel<<<block_count, threads_per_block, threads_per_block * 2 * sizeof(int)>>>(gpu_seq1, gpu_seq2, gpu_f, step, str_count);
		cudaThreadSynchronize();
	}
	cudaEventRecord(end);
	cudaEventSynchronize(end);

	float t;
	cudaEventElapsedTime(&t, start, end);
	printf("dna_alignment_on_gpu: time %f ms; mem %u bytes\n", t, str_count * col_count * sizeof(int));

	cudaFree(gpu_seq1);
	cudaFree(gpu_seq2);

	int *f = (int*) malloc(n * col_count * sizeof(int));
	cudaMemcpy(f, gpu_f, n * col_count * sizeof(int), cudaMemcpyDeviceToHost);
	cudaFree(gpu_f);

	if ((seq1_out != 0) && (seq2_out != 0))
		find_alignment(seq1, n, seq2, m, seq1_out, seq2_out, f, col_count);

	int res = f[col_count * (n - 1) + m - 1];
	free(f);
	return res;
}
